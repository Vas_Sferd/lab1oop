/*
 * main.cxx
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdlib> 
#include <ctime>


// Компоратор
int intcompare(const void * first, const void * second) {	return *(const int*)first > *(const int*)second;}

class LotoBag
{
	private:
		int * barrels_sequence;
		int current;
		int count;
	public:
		LotoBag(int count)
		{	
			srand(time(NULL));
			
			this->barrels_sequence = new int[count];
			this->current = 0;
			this->count = count;	
			
			// Заполняем массив последовательностью
			for (int i = 0; i < count; ++i)
			{
				barrels_sequence[i] = i;
			}
			
			// Перемешиваем
			for (int i = 0; i < count * 4; ++i)
			{
				int a = rand() % count;
				int b = rand() % count;
				
				int tmp = barrels_sequence[a];
				barrels_sequence[a] = barrels_sequence[b];
				barrels_sequence[b] = tmp;
			}
		}
		~LotoBag()
		{
			if (barrels_sequence)
			{
				delete barrels_sequence;
			}
		}
		int getCurrentPos()
		{
			return current;
		}
		int * getArray()
		{
			return barrels_sequence;
		}
		int goNext()
		{
			if (current < count)
			{
				return barrels_sequence[current++];
			}
			else
			{
				return -1;
			}
		}
};

class LotoBilet
{
	private:
		int line_len;		// Длина строки в билете
		int line_count;		// Число строк в билете
		int * values;		// Таблица Массива
	public:
		LotoBilet(int line_count, int line_len)
		{
			if ((line_len | line_count) <= 0)	// Создание пустого объекта
			{
				this->line_len = 0;
				this->line_count = 0;
				this->values = nullptr;
			}
			else
			{
				srand(time(NULL));
				
				this->line_len = line_len;
				this->line_count = line_count;
				this->values = new int[line_len * line_count];
				
				// Генерация значений билета
				for (int j = 0; j < line_len; ++j)	// Обход столбца
				{
					int units = 0;
					
					for (int i = 0; i < line_count; ++i)	// Обход строки
					{
						// Только еденицы
						units += rand() % (10 - units);
						
						// Итоговое число
						values[(i * line_len) + j] = j * 10 + units;
					}
				}
			}
		}
		~LotoBilet()
		{
			if (values)
			{
				delete values;
			}
		}
		bool hasNum(int num)
		{
			for (int i = 0; i < line_count * line_len; ++i)
			{
				if (values[i] == num) return true;
			}
			
			return false;
		}
		bool isWinner(int count, int * taken) // Проверка на выигрыш
		{
			// Делаем копию массива
			int * sorted_taken = new int[count];
			memcpy(sorted_taken, taken, count * sizeof(int));
			
			qsort(sorted_taken, count, sizeof(int), intcompare);
			
			int cur_i = 0;	// Текущее значение из  билета
			
			for (int i = 0; i < count && cur_i < line_count * line_len; ++i)
			{
				if (sorted_taken[i] == values[cur_i]);
				{
					++cur_i; 
				}
			}

			delete sorted_taken;

			return (cur_i == line_count * line_len)
				? true
				: false;
		}
		std::string str()
		{
			std::ostringstream str;
			
			for (int i = 0; i < line_count; ++i)
			{
				str << "{" << i << "}: ";
				
				for (int j = 0; j < line_len; ++j)
				{
					// Перевод значений в строку
					str << values[(i * line_len) + j] << " ";
				}
				
				str << std::endl;
			}
			
			return str.str();
		}
};

int main()
{    
    LotoBilet*	lbilet	= new LotoBilet(3, 9);   
    LotoBag*	lbag	= new LotoBag(100);
       
    int choice;
    
    std::cout << "Succses generate loto bilet"	<< std::endl << std::endl;
    
    while (true)
    {
		std::cout << "0 - Quit"						<< std::endl;
		std::cout << "1 - Get a new barrel"			<< std::endl;
		std::cout << "2 - Check is a winning bilet"	<< std::endl;
		std::cout << "3 - Check num in bilet"		<< std::endl;
		std::cout << "4 - Print bilet"				<< std::endl;
		
		std::cin >> choice;
		std::cout << std::endl;
		
		switch (choice)
		{
			case 0: return 0;
			case 1: std::cout << "Next barrel is " << lbag->goNext() << std::endl; break;
			case 2:
				std::cout << (lbilet->isWinner(1 + lbag->getCurrentPos(), lbag->getArray())
					? "Yeah! You are winner"
					: "You are not winner now")
					<< std::endl;
						break;
			
			case 3:
				int num;
				std::cin >> num;
				std::cout << (lbilet->hasNum(num)
					? "Yes! This num was be founded"
					: "No! This num do not be founded"
					)
					<< std::endl;
						break;
			
			case 4: std::cout << lbilet->str() << std::endl; break;
			default: std::cout << "Incorrect operation"; break;
		}
	}
}
